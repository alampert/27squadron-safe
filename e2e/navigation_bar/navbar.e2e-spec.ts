import { NavbarTestComponent } from './navbar.po';
import { browser } from 'protractor';

describe('The Navbar', () => {
	let page: NavbarTestComponent;

	beforeAll(() => {
		page = new NavbarTestComponent();
		page.navigateTo();
	});

	it('to exist', () => {
		expect(page).toBeTruthy();
	});

	it('should not contain Button dropdown', () => {
		expect(page.getAllText()).not.toContain('Button dropdown');
	});

	describe('navigation', () => {

		describe('About Pages', () => {

			beforeEach(() => {
				page.navigateTo();
				page.clickLink('About');
				expect(page.getElementByCss('#nav_theaircadetprogram')).toBeTruthy();
			});

			it('should navigate to the program page', () => {
				page.clickCss('#nav_theaircadetprogram');
				expect(browser.getCurrentUrl()).toContain('/about/program');
			});

			it('should navigate to the staff page', () => {
				page.clickCss('#nav_staff');
				expect(browser.getCurrentUrl()).toContain('/about/staff');
			});

			it('should navigate to the sponsors page', () => {
				page.clickCss('#nav_sponsors');
				expect(browser.getCurrentUrl()).toContain('/about/sponsors');
			});

			it('should navigate to the community page', () => {
				page.clickCss('#nav_inthecommunity');
				expect(browser.getCurrentUrl()).toContain('/about/community');
			});
		});

		describe('History Pages', () => {

			beforeEach(() => {
				page.navigateTo();
				page.clickLink('History');
				expect(page.getElementByCss('#nav_aircadethistory')).toBeTruthy();
			});

			it('should navigate to the air cadet history page', () => {
				page.clickCss('#nav_aircadethistory');
				expect(browser.getCurrentUrl()).toContain('/history/cadets');
			});

			it('should navigate to the squadron history page', () => {
				page.clickCss('#nav_squadronhistory');
				expect(browser.getCurrentUrl()).toContain('/history/squadron');
			});

			// it('should navigate to the alumni page', () => {
			// 	page.clickCss('#nav_alumni');
			// 	expect(browser.getCurrentUrl()).toContain('/history/alumni');
			// });

			// it('should navigate to the photo gallery page', () => {
			// 	page.clickCss('#nav_photogallery');
			// 	expect(browser.getCurrentUrl()).toContain('/history/photos');
			// });
		});

	});

});
