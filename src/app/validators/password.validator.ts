import { AbstractControl } from '@angular/forms';
export class PasswordValidation {

	static MatchPassword(AC: AbstractControl) {
		if (AC == null || AC.parent == null || AC.parent.get('confirmPassword') == null) {
			return null;
		}

		let password = !!(AC.parent.get('password')) ? AC.parent.get('password').value : ''; // to get value in input tag
		let confirmPassword = !!(AC.parent.get('confirmPassword')) ? AC.parent.get('confirmPassword').value : ''; // to get value in input tag



		if (password != confirmPassword) {
			AC.setErrors({ MatchPassword: true });
			return { MatchPassword: true };
		} else if (confirmPassword == '') {
			AC.setErrors({ MatchPassword: true });
			return { MatchPassword: true };
		} else {
			return null
		}
	}
}
