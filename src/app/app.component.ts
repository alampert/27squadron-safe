import { Component } from "@angular/core";

/**
 * Root app component. Title set to "27 Air Cadets"
 */
@Component (
	{
		selector    : "app-root",
		templateUrl : "./app.component.html",
		styleUrls   : [ "./app.component.css" ]
	}
)

export class AppComponent {
	/**
	 * Title of program
	 * @type {string}
	 */
	title = "27 Air Cadets";
}
