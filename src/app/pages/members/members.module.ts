import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MemberDashboardPage } from './pages/dashboard/dashboard.page';
import { AvailabilitiesPage } from './pages/availabilities/availabilities.page';
import { MembersComponent } from './members.component';
import { MemberRoutingModule } from './members-routing.module';
import { MemberNavigationComponent } from './components/member-navigation/member-navigation.component';
import { CommonModule } from '@angular/common';
import { SupplyPage } from './pages/supply/supply.page';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ProfileManagerPage } from './pages/profile-manager/profile-manager.page';
import { MemberEditorComponent } from './components/member-editor/member-editor.component';
import { SharedModule } from '../../components/shared.module';
import { ConfirmDeleteComponent } from './components/confirm-event-delete/confirm-delete.component';
import { MemberListsPage } from './pages/member-lists/member-lists.page';
import { ChangePasswordComponent } from 'app/pages/members/components/change-password/change-password.component';
import { PermissionFormPage } from 'app/pages/members/pages/permission-form/permission-form.page';
import { EventsService } from 'app/services/events/events.service';

@NgModule({
	imports: [
		MemberRoutingModule,
		CommonModule,
		RouterModule,
		FormsModule,
		SharedModule
	],
	declarations: [
		MembersComponent,
		MemberNavigationComponent,

		MemberDashboardPage,
		AvailabilitiesPage,
		SupplyPage,
		ProfileManagerPage,
		MemberEditorComponent,
		ConfirmDeleteComponent,
		MemberListsPage,
		PermissionFormPage
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
	bootstrap: [MembersComponent],
	providers: [
		EventsService
	]
})
export class MembersModule {
}
