import { FormGroup } from '@angular/forms';
import { OnInit, Component, Input } from '@angular/core';
import { QuestionBase } from 'app/components/questions/question-base';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { PasswordQuestion } from 'app/components/questions/question-password';
import { ToastyService } from 'ng2-toasty';
import { UserService } from 'app/services/user/user.service';
import { UserModel } from 'app/models/user.model';

@Component({
	selector: 'app-change-password',
	templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

	public passwordFormGroup: FormGroup;
	public questions: QuestionBase<any>[];
	@Input() user: UserModel;
	private submittingForm = false;

	constructor(
		private questionControlService: QuestionControlService,
		private toastyService: ToastyService,
		private userService: UserService
	) {
		this.updateForm();
	}

	ngOnInit() {
		this.updateForm();
	}

	save(passwordJSON?: string) {
		try {
			let passwordObj = JSON.parse(passwordJSON);

			if (passwordObj.password != passwordObj.confirmPassword) {
				throw new Error('Password and Confirm Password did not match');
			}

			if (!(this.user) || !(this.user.member_id) || !(this.user.email)) {
				throw new Error('Member was not passed in properly');
			}

			if (!this.submittingForm) {

				this.submittingForm = true;

				let updatePassSub = this.userService.updatePassword(this.user.member_id, this.user.email, passwordObj.password).subscribe(res => {
					if (!!res) {
						if (res.error) {
							throw new Error(res.error);
						} else {
							this.toastyService.success('Password Updated!');
							updatePassSub.unsubscribe();
						}
					}
					this.submittingForm = false;
				});
			}
		} catch (e) {
			console.error(e);
			this.toastyService.error('Error Updating Password!');
			this.submittingForm = false;
		}
	}

	updateForm() {
		this.questions = [

			new PasswordQuestion({
				label: 'Password',
				key: 'password',
				value: ''
			}),
			new PasswordQuestion({
				label: 'Confirm Password',
				key: 'confirmPassword',
				value: ''
			}),

		];

		// this.questions.sort((a, b) => a.order - b.order);

		this.passwordFormGroup = this.questionControlService.toFormGroup(this.questions);
	}

}
