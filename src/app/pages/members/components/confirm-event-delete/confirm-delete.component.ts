import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
	selector: 'app-confirm-delete',
	templateUrl: './confirm-delete.component.html',
	styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent implements OnInit {

	constructor(public dialogRef: MatDialogRef<ConfirmDeleteComponent>) { }

	ngOnInit() {
	}

	close(event, action: boolean) {
		event.preventDefault();
		this.dialogRef.close(action);
	}

}
