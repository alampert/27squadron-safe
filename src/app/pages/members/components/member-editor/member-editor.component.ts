import { Component, OnInit, Input } from '@angular/core';
import { UserModel } from '../../../..//models/user.model';
import { UserService } from '../../../../services/user/user.service';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { RankModel } from '../../../../models/rank.model';
import { Observable } from 'rxjs/Rx';
import { AuthUserService } from '../../../../services/auth-user/auth-user.service';
import { AuthStaffService } from 'app/services/auth-user/auth-staff.service';
import { GroupModel } from 'app/models/group.model';
import { GroupsService } from 'app/services/groups/groups.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { Header2Question } from 'app/components/questions/question-header2';
import { TextboxQuestion } from 'app/components/questions/question-textbox';
import { PasswordQuestion } from 'app/components/questions/question-password';
import { Header1Question } from 'app/components/questions/question-header1';
import { DropdownQuestion } from 'app/components/questions/question-dropdown';
import { environment } from 'environments/environment';
import { ButtonQuestion } from 'app/components/questions/question-button';
import { NumberQuestion } from 'app/components/questions/question-number';
import { HiddenQuestion } from 'app/components/questions/question-hidden';
import { MatDialog } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';

@Component({
	selector: 'app-member-editor',
	templateUrl: './member-editor.component.html',
	styleUrls: ['./member-editor.component.css']
})
export class MemberEditorComponent implements OnInit {

	member_id: number;
	@Input() user: UserModel;
	canEditRank: boolean;
	canEditLevel: boolean;
	canEditGroupMembership: boolean;
	ranks: RankModel[];
	groups: GroupModel[];


	public loginFormGroup: FormGroup;
	public questions: QuestionBase<any>[];

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private userService: UserService,
		private toastyService: ToastyService,
		private toastyConfig: ToastyConfig,
		private authUser: AuthUserService,
		private authStaff: AuthStaffService,
		private groupsService: GroupsService,
		private formBuilder: FormBuilder,
		private questionControlService: QuestionControlService,
		private dialog: MatDialog
	) {
		this.user = new UserModel;
		this.user.rank = new RankModel;
		this.ranks = new Array<RankModel>();
		this.updateForm();
		this.dialog.open(LoadingModalComponent);
	}

	ngOnInit() {
		this.updatePage();
	}

	// tslint:disable-next-line:use-life-cycle-interface
	ngOnChanges() {
		this.updatePage();
	}

	save(userDataJson?: string) {

		try {

			let updatedUser

			if (userDataJson != null && userDataJson != '') {

				updatedUser = JSON.parse(userDataJson);

				if (updatedUser.member_id == null) {
					throw new Error('member id was not defined');
				}
			} else {
				updatedUser = this.user;
			}

			this.authUser.invalidateStoredUser();
			let saveSub = this.userService.updateUser(updatedUser).subscribe(res => {
				if (res) {
					this.updatePage();
					this.toastyService.success('Saved!');
					// this.updatePermissions();
					saveSub.unsubscribe();
				}
			}, error => {
				this.toastyService.error(`Failed to Save!`);
				console.error(error);
				console.error(`JSON: `, userDataJson);
			});

		} catch (e) {
			this.toastyService.error(`Failed to Save!`);
			console.error(e);
			console.error(`JSON: `, userDataJson);
		}

	}

	updatePermissions() {
		this.authStaff.canActivate().subscribe(res => {
			this.canEditRank = res;
			this.canEditLevel = res;
			this.canEditGroupMembership = false; // Edit group membership through group editor
		});
	}

	getMemberId(): Observable<number> {
		if (this.user.member_id == null) {
			return this.route.params.map(params => {
				if (+params['id'] != null && !isNaN(+params['id'])) {
					return +params['id'];
				} else {
					return null;
				}
			});
		} else {
			return new Observable(subscriber => {
				subscriber.next(this.user.member_id);
				subscriber.complete();
			})
		}
	}

	getUserGroups(member_id: number): Observable<GroupModel[]> {
		return this.groupsService.getAllGroups(member_id).map(groups => this.groups = groups);
	}

	getUser(member_id: number) {
		return this.userService.getUserById(member_id).map(user => this.user = user);
	}

	filterRanks(rankArray: RankModel[]) {

		if (typeof rankArray !== 'undefined' || rankArray !== null) {

			return rankArray.filter(rank => {
				if (typeof rank !== 'undefined') {
					if (typeof rank.branch !== 'undefined') {
						return rank.branch !== 'CIC_old';
					} else {
						return true;
					}
				} else {
					return false;
				}
			});

		}
	}

	updatePage() {
		try {

			this.dialog.open(LoadingModalComponent);

			// getMemberId obtains the member id from the URL, or stored data
			let userInfoObs = this.getMemberId().switchMap(member_id => {

				if (member_id != null && typeof member_id !== 'undefined' && !isNaN(member_id)) {
					return Observable.forkJoin([
						this.getUser(member_id),		// Gets user based on ID
						this.getUserGroups(member_id),	// Gets groups user is in
						this.userService.getAllRanks()	// Gets all ranks
					])
				} else {
					return new Observable(subscriber => {
						subscriber.next([null, null, null]);
					});
				}

			});

			userInfoObs.subscribe(returnedData => {
				// applying any to get rid of errors later on
				let data = (<any[]>returnedData);

				// Storing all ranks
				// tslint:disable-next-line:curly
				if (data[2] != null) this.ranks = this.filterRanks(<RankModel[]>data[2]);

				// Enabling / Disabling page features
				this.updatePermissions();

				this.updateForm();

				this.dialog.closeAll();

				return returnedData;
			});


		} catch (e) {
			console.error(e);
			this.toastyService.error(e.message);
			this.dialog.closeAll();
		}
	}

	updateForm() {
		let userInfoQuestions = [

			new HiddenQuestion({
				label: 'Member ID',
				key: 'member_id',
				value: this.user.member_id,
			}),

			new Header1Question({
				label: 'User Information',
				order: 0
			}),

			new DropdownQuestion({
				key: 'rank_id',
				label: 'Rank',
				value: this.user.rank_id,
				required: true,
				order: 1,
				disabled: environment.disableForms && !this.canEditRank,
				options: this.ranks.map(rank => {
					return { key: rank.rank_id, value: `${rank.rank_text_full} (${rank.branch} - ${!!rank.element ? rank.element : 'All'})` }
				})
			}),

			new TextboxQuestion({
				key: 'fname',
				label: 'First Name',
				value: this.user.fname,
				required: true,
				order: 2,
				disabled: environment.disableForms
			}),

			new TextboxQuestion({
				key: 'lname',
				label: 'Last Name',
				value: this.user.lname,
				required: true,
				order: 3,
				disabled: environment.disableForms
			}),

		];

		let contactInfoQuestions = [

			new Header1Question({
				label: 'Contact Information',
				order: 4
			}),

			new TextboxQuestion({
				key: 'email',
				label: 'E-Mail',
				value: this.user.email,
				required: true,
				isEmail: true,
				disabled: environment.disableForms
			}),

			new NumberQuestion({
				key: 'phone_number',
				label: 'Phone Number',
				value: this.user.phone_number,
				disabled: environment.disableForms
			})
		];

		let squadronInformation: QuestionBase<any>[] = [
			new Header1Question({
				label: 'Squadron Information'
			}),
			new HiddenQuestion({
				key: 'level',
				value: '0'
			})
		];

		if (!!(this.user.rank) && this.user.rank.branch == 'Cadet') {
			let levelIndex = squadronInformation.findIndex(q => q.key == 'level');

			if (levelIndex != -1) {
				squadronInformation.splice(levelIndex, 1, new NumberQuestion({
					key: 'level',
					label: 'Level',
					value: !!(this.user.level) ? this.user.level : 1,
					disabled: environment.disableForms && !this.canEditLevel
				}));
			}
		}

		this.questions = [];
		this.questions = this.questions.concat(userInfoQuestions, contactInfoQuestions, squadronInformation)

		// this.questions.sort((a, b) => a.order - b.order);

		this.loginFormGroup = this.questionControlService.toFormGroup(this.questions);
	}

}
