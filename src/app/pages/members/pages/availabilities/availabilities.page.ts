import { Component, OnInit } from '@angular/core';
import { EventAvailModel } from '../../../../models/event.model';
import { AvailabilityStatus } from '../../../../globals';
import { AvailabilitiesService } from '../../../../services/availabilities/availabilities.service';
import { UserService } from '../../../../services/user/user.service';
import { AuthUserService } from '../../../../services/auth-user/auth-user.service';
import { ToastyService } from 'ng2-toasty';
import { AuthMember } from '../../../../services/auth-user/auth-member.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from '../../../../components/loading-modal/loading-modal.component';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

/**
 * The member availabilities page displays a list of events to all members, and allows members to sign up for events. The list of events also displays additional information to the members such as the maximum number of cadets, if the event is public, parent permission requirement, and so on.
 */
@Component({
	selector: 'app-availabilities',
	templateUrl: 'availabilities.page.html',
	styleUrls: ['availabilities.page.css'],
	providers: [
		AvailabilitiesService,
		UserService,
		AuthUserService,
		ToastyService
	]
})
export class AvailabilitiesPage implements OnInit {

	public availabilityStatus;
	private user;

	/**
	 * List of events to be displayed. Must have an event id.
	 */
	events: EventAvailModel[];

	/**
	 * Initializes the event list, and injectables
	 */
	constructor(
		private availService: AvailabilitiesService,
		private userService: AuthUserService,
		private authMember: AuthMember,
		private dialog: MatDialog,
		private toastyService: ToastyService,
		private router: Router
	) {
		this.events = new Array<EventAvailModel>();
		this.availabilityStatus = AvailabilityStatus;
		this.dialog.open(LoadingModalComponent);
	}

	/**
	 * Initializes the availabilities page by calling the appropriate functions.
	 */
	ngOnInit() {

		let userServiceSub = this.userService.getUser(this.authMember.getMemberId()).subscribe(data => {

			if (data) {
				this.user = data;

				let eventListSub = this.getEventList().subscribe(eventList => {
					this.dialog.closeAll();
					userServiceSub.unsubscribe();
					eventListSub.unsubscribe();
				});
			}

		});
	}

	/**
	 * Retrieves a list of events from service, and sets it to this.events
	 */
	private getEventList() {

		if (this.user == null) {
			// throw new Error('this.user is undefined');
			this.dialog.closeAll();
			console.error(`this.user was undefined`);
			this.toastyService.error('Error loading user. Please re-login!');
		}

		return this.availService.getMemberEventAvailabilities(this.user.member_id).map(data => {
			this.events = data;
			return data;
		});
	}

	public canApplyForEvent(event: EventAvailModel): boolean {

		const unsetStatus = (+event.status === AvailabilityStatus.UNSET);

		return (unsetStatus || event.can_change) && this.enoughCadetSpots(event);
	}

	public enoughCadetSpots(event: EventAvailModel): boolean {
		return (event.other_cadets_attending < +event.max_cadet_count) || +event.max_cadet_count === 0;
	}

	/**
	 * When a member clicks on the "apply for event" button, this function is called.
	 * Function sends service request to set availability status to YES
	 * @param event_id
	 */
	private applyForEvent(event_id: number) {
		let userSub = this.userService.getUser().subscribe(user => {
			let availSub = this.availService.availabilityStatusUpdate(event_id, user.member_id, AvailabilityStatus.YES).subscribe(data => {
				if (data) {
					this.events.find(x => x.event_id === event_id).status = AvailabilityStatus.YES;
					availSub.unsubscribe();
					userSub.unsubscribe();
				}
			});
		});
	}

	/**
	 * When a member clicks on the button to decline an event, this function is aclled.
	 * Function sends a service request to set the availability status to NO.
	 * @param event_id
	 */
	private decliningEvent(event_id: number) {
		let userSub = this.userService.getUser().subscribe(user => {
			let availSub = this.availService.availabilityStatusUpdate(event_id, user.member_id, AvailabilityStatus.NO)
				.subscribe(data => {
					if (data) {
						this.events.find(x => x.event_id === event_id).status = AvailabilityStatus.NO;
						availSub.unsubscribe();
						userSub.unsubscribe();
					}
				});
		});

	}

	public openPermissionForm(event_id: number) {
		this.router.navigate(['members', 'permission-form', event_id]);
	}

}
