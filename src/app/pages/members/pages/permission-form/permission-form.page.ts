import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventsService } from 'app/services/events/events.service';
import { EventModel } from 'app/models/event.model';
import { UserModel } from 'app/models/user.model';
import { AuthUserService } from 'app/services/auth-user/auth-user.service';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment'
import { UserService } from 'app/services/user/user.service';

@Component({
	templateUrl: 'permission-form.page.html',
	styleUrls: [
		'permission-form.page.scss'
	]
})
export class PermissionFormPage implements OnInit {

	public eventId: number;
	public event: EventModel;
	public user: UserModel;
	public environment = environment;
	public today: Date;
	public isSameDay = false;
	public startDate: Date;
	public endDate: Date;
	public opi: UserModel;

	constructor(
		private route: ActivatedRoute,
		private eventsService: EventsService,
		private authUserService: AuthUserService,
		private userService: UserService
	) {
	}

	ngOnInit() {
		this.today = new Date();
		this.eventId = this.route.snapshot.params['id'];
		// this.isSameDay = (new Date(this.event.start_date_time)).getUTCDay;

		Observable.combineLatest(
			this.eventsService.getEvent(this.eventId),
			this.authUserService.getUser()
		).subscribe(data => {
			if (!!data) {
				[this.event, this.user] = data;

				// Converting start and end date strings to objects
				this.isSameDay = this.eventsService.isSameDay(this.event);
				this.startDate = new Date(this.event.start_date_time);
				this.endDate = new Date(this.event.end_date_time);

				if (!!(this.event.opi_member_id)) {
					this.userService.getUserById(this.event.opi_member_id).subscribe(opi => this.opi = opi);
				}
			}
		});
	}

}
