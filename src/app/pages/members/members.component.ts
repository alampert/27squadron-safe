import { Component, OnInit } from "@angular/core";

/**
 * Root component for the member module. Wraps the entire members section with a second navigational bar for the member pages only.
 */
@Component ( {
	templateUrl : "./members.component.html"
} )
export class MembersComponent implements OnInit {

	/**
	 * Constructor does nothing at the moment
	 */
	constructor() {
	}

	/**
	 *  NgOnInit does nothing at the moment
	 */
	ngOnInit() {
	}

}
