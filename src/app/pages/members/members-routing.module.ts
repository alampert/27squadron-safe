/**
 * Created by andrew on 23/04/17.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemberDashboardPage } from './pages/dashboard/dashboard.page';
import { AvailabilitiesPage } from './pages/availabilities/availabilities.page';
import { MembersComponent } from './members.component';
import { StaffComponent } from './staff/staff.component';
import { EventManagerPage } from './staff/event-manager/event-manager.page';
import { SupplyPage } from './pages/supply/supply.page';
import { EventEditorComponent } from './staff/event-manager/event-editor/event-editor.component';
import { ProfileManagerPage } from './pages/profile-manager/profile-manager.page';
import { AuthMember } from '../../services/auth-user/auth-member.service';
import { AuthUserService } from '../../services/auth-user/auth-user.service';
import { AuthStaffService } from '../../services/auth-user/auth-staff.service';
import { SignInAuthenticatorPage } from './staff/sign-in-authenticator/sign-in-authenticator.page';
import { EventRosterComponent } from './staff/event-manager/event-roster/event-roster.component';
import { MemberManagerPage } from './staff/member-manager/member-manager.page';
import { MemberEditorComponent } from './components/member-editor/member-editor.component';
import { TrainingManagerComponent } from 'app/pages/members/staff/training-manager/training-manager.component';
import { TrainingEventEditorComponent } from 'app/pages/members/staff/training-manager/training-event-editor/training-event-editor.component';
import { MemberListsPage } from './pages/member-lists/member-lists.page';
import { PermissionFormPage } from 'app/pages/members/pages/permission-form/permission-form.page';

/**
 * Routing for the members pages.
 *
 * url.com/members and url.com/members/login redirects to url.com/login.
 */
const memberRoutes: Routes = [
	{
		path: 'members',
		component: MembersComponent,
		canActivate: [AuthMember],
		children: [
			{ path: '', redirectTo: '/login', pathMatch: 'full' },
			{ path: 'login', redirectTo: '/login', pathMatch: 'full' },
			{ path: 'dashboard', component: MemberDashboardPage },
			{ path: 'availabilities', component: AvailabilitiesPage },
			{
				path: 'permission-form/:id', component: PermissionFormPage
			},
			{ path: 'supply', component: SupplyPage },
			{ path: 'profile', component: ProfileManagerPage },
			{
				path: 'staff',
				canActivate: [AuthStaffService],
				children: [
					{ path: '', component: StaffComponent },
					{
						path: 'event-manager',
						children: [
							{ path: '', component: EventManagerPage },
							{ path: 'event-editor/:id', component: EventEditorComponent },
							{ path: 'event-roster/:id', component: EventRosterComponent },
						]
					},
					{ path: 'sia', component: SignInAuthenticatorPage },
					{ path: 'member-manager', component: MemberManagerPage },
					{ path: 'member-manager/member-editor/:id', component: MemberEditorComponent },
					{
						path: 'training-manager',
						canActivate: [AuthStaffService],
						children: [
							{ path: '', component: TrainingManagerComponent },
							{ path: 'training-event-editor/:trgeventid', component: TrainingEventEditorComponent }
						]
					},
					{ path: 'lists', component: MemberListsPage }
				]
			}
		]
	}
];
@NgModule({
	imports: [
		RouterModule.forChild(memberRoutes)
	],
	exports: [
		RouterModule
	], providers: [
		AuthMember,
		AuthUserService
	]
})
export class MemberRoutingModule { }
