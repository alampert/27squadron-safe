import { Component, OnInit } from '@angular/core';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { UserService } from 'app/services/user/user.service';
import { UserModel, SearchUserModel } from 'app/models/user.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LoadingModalComponent } from 'app/components/loading-modal/loading-modal.component';
import { Observable } from 'rxjs/Observable';

@Component({
	selector: 'app-member-manager',
	templateUrl: 'member-manager.page.html',
	styleUrls: ['member-manager.page.css'],
	providers: [UserService]
})
export class MemberManagerPage implements OnInit {

	public members: UserModel[];
	public filteredMembers: UserModel[];
	public newMember: UserModel;
	public disableAddUser: boolean;
	public searchParam: string;

	constructor(private toastyService: ToastyService, private toastyConfig: ToastyConfig, private userService: UserService, private dialog: MatDialog) {
		this.toastyConfig.theme = 'material';
		this.dialog.open(LoadingModalComponent);
	}

	ngOnInit() {

		let loadSub = this.loadUsers().subscribe(users => {
			if (!!users) {
				loadSub.unsubscribe();
			}
		})

		this.newMember = new UserModel;
		this.disableAddUser = false;

		this.searchParam = '';
	}

	submitNewUser() {
		this.userService.updateUser(this.newMember).subscribe(res => console.log(res));
	}

	loadUsers(): Observable<any[]> {
		return this.userService.listOfUsers().map(users => {
			return users.map(user => {

				let tmpUser = user;

				// TODO: If user is not active, set filtered = true;
				tmpUser.filtered = false;

				this.dialog.closeAll();

				return tmpUser;

			});
		}).map(users => {
			this.members = users;
			this.filteredMembers = users;
			return users;
		});
	}

	verifyFields() {
		let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		if (re.test(this.newMember.email)) {
			this.disableAddUser = false;
		} else {
			this.disableAddUser = true;
		}
	}

	search(searchParam: any) {

		this.filteredMembers = this.members.filter(user => {
			let userString = '';

			// Appending all values one after the other
			Object.keys(user).forEach(key => {
				userString += ' ' + user[key];
			});

			// Appending values after keys
			Object.keys(user).forEach(key => {
				userString += key + ' ' + user[key] + ' ';
			});

			// Common querries
			// Rank fname lname
			userString += user.rank.rank_text_short + ' ' + user.fname + ' ' + user.lname + ' ';
			// Rank lname lname
			userString += user.rank.rank_text_short + ' ' + user.lname + ' ';
			// {Rank} {level}
			userString += user.rank.rank_text_short + ' ' + user.level + ' ';

			return userString.toLowerCase().indexOf(searchParam.toLowerCase()) !== -1;
		});
	}

	initializeUser(member: SearchUserModel) {
		let initSub = this.userService.initializeMember(member.member_id).flatMap(res => {
			if (!!(res)) {
				return this.loadUsers();
			}
		}).subscribe(res => {
			if (!!res) {
				initSub.unsubscribe();
			}
		});
	}

}
