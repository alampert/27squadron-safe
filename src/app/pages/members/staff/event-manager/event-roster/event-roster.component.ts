import { Component, OnInit, Input } from '@angular/core';
import { EventModel } from '../../../../../models/event.model';
import { ActivatedRoute, Router } from '@angular/router';
import { EventsService } from '../../../../../services/events/events.service';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { AvailabilitiesService } from '../../../../../services/availabilities/availabilities.service';
import { AvailabilityStatus } from '../../../../../globals';

@Component ( {
	selector    : 'app-event-roster',
	templateUrl : 'event-roster.component.html',
	styleUrls   : [ 'event-roster.component.css' ],
	providers   : [
		EventsService,
		AvailabilitiesService
	]
} )
export class EventRosterComponent implements OnInit {

	event : EventModel;
	availStatus : any;
	avails : any[];

	constructor( private route : ActivatedRoute, private router : Router, private eventsService : EventsService,
		private toastyService : ToastyService, private toastyConfig : ToastyConfig,
		private availService : AvailabilitiesService ) {
		this.event              = new EventModel;
		this.toastyConfig.theme = 'material';
		this.availStatus = AvailabilityStatus;
	}

	ngOnInit() {
		this.getEvent ();
	}

	getEvent() {
		// retrieving route params
		this.route.params.subscribe ( params => {

			// if id is passed in, retrieve event
			if ( +params[ 'id' ] != null ) {
				this.eventsService.getEvent ( +params[ 'id' ] ).subscribe ( event => {
					if ( event ) {
						this.event = event;
						this.getAttendance ();
					}
				} );
			}
		} );
	}

	getAttendance() : void {
		this.availService.getEventAvails ( this.event.event_id ).subscribe ( avails => {
			this.avails = avails;
		} );
	}

}
