import { Component, OnInit, Input } from '@angular/core';
import { TrainingEventModel, EventModel } from 'app/models/event.model';
import { UserModel, BaseUserModel } from 'app/models/user.model';
import { Observable } from 'rxjs/Observable';
import { UserService } from 'app/services/user/user.service';
import { EventsService } from 'app/services/events/events.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainingService } from 'app/services/training/training.service';
import { ToastyService } from 'ng2-toasty';

@Component({
	selector: 'app-training-event-editor',
	templateUrl: './training-event-editor.component.html',
	styleUrls: ['./training-event-editor.component.css'],
	providers: [EventsService]
})
export class TrainingEventEditorComponent implements OnInit {

	@Input() public trg_event_id: number;
	public trainingEvent: TrainingEventModel;
	public availableInstructorsArray: BaseUserModel[];
	public selectedInstructor: BaseUserModel;

	public showInstructorDragAndDrop = false;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private userService: UserService,
		private eventsService: EventsService,
		private trainingService: TrainingService,
		private toastyService: ToastyService
	) {
		this.availableInstructorsArray = new Array<BaseUserModel>();
		this.trainingEvent = new TrainingEventModel();
	}

	ngOnInit() {

		try {

			this.getTrainingEventId().flatMap(eventId => {

				return this.getTrainingEvent(eventId).map(event => {
					this.trainingEvent = event;
					return event;
				});

			}).subscribe();

		} catch (e) {
			console.error(e.message);
			this.toastyService.error(e.message);
		}
	}

	updateAvailableInstructors(): Observable<any> {
		return this.userService.getInstructors().map(users => {
			if (users != null) {
				return users.filter(user => {
					if (this.selectedInstructor != null) {
						return user.member_id !== this.selectedInstructor.member_id;
					} else {
						return true;
					}
				});
			} else {
				throw new Error('Returned users from getInstructors() was null!');
			}
		});
	}

	onItemDrop(e: any) {
		// Get the dropped data here
		this.selectedInstructor = e.dragData;
		this.updateAvailableInstructors();

	}

	getTrainingEvent(event_id: number): Observable<TrainingEventModel> {
		return this.trainingService.getEvent(event_id);
	}

	getTrainingEventId(): Observable<number> {
		return this.route.params.map(params => {
			if (+params['trgeventid'] != null && !isNaN(+params['trgeventid'])) {
				let returnVal = +params['trgeventid'];
				return returnVal;
			} else {
				throw new Error('Event ID was not set in URL');
			}
		});
	}

}
