import { Component, OnInit } from '@angular/core';
import { SearchUserModel } from '../../../../models/user.model';
import { UserService } from '../../../../services/user/user.service';
import { AttendanceStatus } from '../../../../globals';

@Component({
	selector: 'app-sign-in-authenticator',
	templateUrl: 'sign-in-authenticator.page.html',
	styleUrls: ['sign-in-authenticator.page.css'],
	providers: [UserService]
})
export class SignInAuthenticatorPage implements OnInit {

	public searchQuery: string;
	public listOfUsers: SearchUserModel[];
	public filteredUsers: SearchUserModel[];
	public challenge: string;
	public attendanceStatus: any;

	constructor(private userService: UserService) {
		this.searchQuery = '';
		this.attendanceStatus = AttendanceStatus;
	}

	ngOnInit() {
		this.userService.listOfUsers().subscribe(list => {
			this.listOfUsers = list;
			this.listOfUsers.forEach(user => {
				user.showChallenge = false;
			});
		});
	}

	queryUpdate() {
		if (this.searchQuery == null) {
			this.filteredUsers = Object.assign([], this.listOfUsers);
		}

		this.filteredUsers = Object.assign([], this.listOfUsers)
			.filter(user => {
				let searchString = user.rank.rank_text_full + ' ' + user.rank.rank_text_short + ' ' + user.lname + ' '
					+ user.fname + ' ' + user.lname;

				return searchString.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1
			});

	}

	startSignInProcess(member_id: number) {
		// Showing challenge for only last user that was entered
		this.challenge = '';
		this.listOfUsers.forEach(user => {
			user.showChallenge = (
				user.member_id === member_id
			);
		});
	}

	submitChallengeAnswer(member_id: number) {
		console.debug(this.challenge);

		let challengePassed = true;

		if (challengePassed) {
			this.listOfUsers.find(x => x.member_id === member_id).status = AttendanceStatus.PRESENT;
		}

		// Resetting
		this.startSignInProcess(-1);	// turn off all challenge fields
		this.challenge = '';
		this.searchQuery = '';
		this.queryUpdate();
	}

}
