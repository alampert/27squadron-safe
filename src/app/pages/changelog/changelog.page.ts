import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-changelog',
	templateUrl: './changelog.page.html',
	styleUrls: ['./changelog.page.css']
})
export class ChangeLogPage implements OnInit {

	public changeList: any[];

	constructor() { }

	ngOnInit() {
		this.changeList = [
			{
				version: '0.1.0',
				changes: [
					'Added the change log',
					'Added routing for version number to change log page.'
				]
			},
			{
				version: '0.1.1',
				changes: [
					'Reduced footer font size to 60%',
				]
			},
			{
				version: '0.2.0',
				changes: [
					'Added angular material design',
					'Added loading bar for login screen',
					'Added date picker for event manager'
				]
			},
			{
				version: '0.3.0',
				changes: [
					'Added field in event editor for cadet and staff uniforms',
					'Added inserted uniform setting below date/time in availabilities card'
				]
			},
			{
				version: '0.3.1',
				changes: [
					'Added home page loading circle',
				]
			},
			{
				version: '0.3.2 & 0.3.3',
				changes: [
					'Style updates',
				]
			},
			{
				version: '0.3.4',
				changes: [
					'Adding in event deletion confirmation dialogue window',
					'Fixing date picker button placement in event manager',
					'Removing past events from availabilitiesr',
					'Ordering availabilities by start date',
					'Ordering events in event manager by start date',
				]
			},
			{
				version: '0.3.5',
				changes: [
					'Removing links that have not been fully implemented yet.',
					'Forcing package.json dependency versions by setting each to "="',
					'Loading spinners for most pages'
				]
			},
			{
				version: '0.3.6',
				changes: [
					'Fixed member nav for mobile users : collapsed member navigation was not showing for users'
				]
			},
			{
				version: '0.3.7',
				changes: [
					'Added more information to the member manager table',
					'Added search bar with the ability to search based on fields, or rank and level'
				]
			},
			{
				version: '0.3.8',
				changes: [
					'Added ability to edit users level from the profile editor page',
					'Added list of groups, and ability for users to see which groups they are members of'
				]
			}
		];
	}

}
