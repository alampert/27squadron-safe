import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HistoryRoutingModule } from "./history-routing.module";
import { SharedModule } from "../../components/shared.module";
import { HistoryComponent } from "./history.component";
import { HistoryCadetPage } from "./cadet/history.cadet.page";
import { HistorySquadronPage } from "./squadron/squadron.page";
import { HistoryAlumniPage } from "./sponsors/alumni.page";
import { HistoryGalleryPage } from './gallery/gallery.page';

@NgModule ( {
	imports         : [
		CommonModule, HistoryRoutingModule, SharedModule
	], declarations : [
		HistoryComponent, HistoryCadetPage, HistorySquadronPage, HistoryAlumniPage, HistoryGalleryPage
	], schemas      : [ CUSTOM_ELEMENTS_SCHEMA ]
} )
export class HistoryModule {
}
