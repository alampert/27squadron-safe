import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HistoryCadetPage } from "./cadet/history.cadet.page";
import { HistoryComponent } from "./history.component";
import { HistorySquadronPage } from "./squadron/squadron.page";
import { HistoryAlumniPage } from "./sponsors/alumni.page";
import { HistoryGalleryPage } from "./gallery/gallery.page";

const historyRoutes : Routes = [
	{
		path : 'history', component : HistoryComponent, children : [
		{ path : 'cadets', component : HistoryCadetPage },
		{ path : 'squadron', component : HistorySquadronPage },
		{ path : 'alumni', component : HistoryAlumniPage },
		{ path : 'gallery', component : HistoryGalleryPage }
	]
	}
];

@NgModule ( {
	imports    : [
		RouterModule.forChild ( historyRoutes )
	], exports : [
		RouterModule
	]
} )
export class HistoryRoutingModule {
}
