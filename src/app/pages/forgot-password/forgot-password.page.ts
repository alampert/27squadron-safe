import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastyConfig, ToastyService, ToastOptions } from 'ng2-toasty';
import { AuthMember } from '../../services/auth-user/auth-member.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QuestionBase } from 'app/components/questions/question-base';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { TextboxQuestion } from 'app/components/questions/question-textbox';
import { PasswordQuestion } from 'app/components/questions/question-password';
import { Header1Question } from 'app/components/questions/question-header1';
import { Header2Question } from 'app/components/questions/question-header2';
import { environment } from 'environments/environment';
import { ButtonQuestion } from 'app/components/questions/question-button';
import { UserService } from 'app/services/user/user.service';

/**
 * The login page allows members to login with a username and password. Provides access to the members module.
 */
@Component({
	selector: 'app-forgot-password',
	templateUrl: 'forgot-password.page.html',
	styleUrls: ['forgot-password.page.css'],
})
export class ForgotPasswordPage implements OnInit {

	public loading: boolean;
	public forgotPasswordForm: FormGroup;
	public questions: QuestionBase<any>[];
	public submittingForm = false;

	/**
	 * Constructor provides access to the angular's router module.
	 * @param router
	 */
	constructor(
		private router: Router,
		private toastyService: ToastyService,
		private userService: UserService,
		private toastyConfig: ToastyConfig,
		private formBuilder: FormBuilder,
		private questionControlService: QuestionControlService
	) {
		this.toastyConfig.theme = 'material';
		this.toastyConfig.position = 'top-right';
		this.loading = false;
	}

	/**
	 * ngOnInit does nothing at the moment.
	 */
	ngOnInit() {
		this.questions = [

			new Header2Question({
				label: 'Password Recovery'
			}),

			new TextboxQuestion({
				key: 'username',
				label: 'E-Mail',
				value: '',
				required: true,
				order: 2,
				isEmail: true,
				disabled: environment.disableForms
			})
		];

		this.questions.sort((a, b) => a.order - b.order);

		this.forgotPasswordForm = this.questionControlService.toFormGroup(this.questions);
	}

	/**
	 * Function that is called when the user attempts to login.
	 */
	submitPasswordRecovery(formJson: string) {

		let formData = JSON.parse(formJson);
		this.loading = true;

		let badLoginToast: ToastOptions = {
			title: 'Failed Password Recovery',
			msg: 'Check your username, and initialize your account with an officer'
		};

		if (!!(formData) && !this.submittingForm) {

			this.submittingForm = true;

			try {

				let passwordRecSub = this.userService.passwordRecovery(formData.email)
					.debounceTime(6000)
					.subscribe(res => {
						if (!!res) {
							if (res._body.indexOf('Sorry') != -1) {
								this.toastyService.error(badLoginToast);
								passwordRecSub.unsubscribe();
								this.submittingForm = false;
								throw new Error('Server Error');
							}

							this.toastyService.success({
								title: 'Submitted!',
								msg: 'Your request has been submitted. Please check your email for further instructions'
							});
							passwordRecSub.unsubscribe();
							this.submittingForm = false;
						}
					}, error => {
						this.toastyService.error(badLoginToast);
						this.submittingForm = false;
					});

			} catch (e) {
				this.toastyService.error(badLoginToast);
				console.error(e);
				this.loading = false;
				this.submittingForm = false;
			}

		}

	}

}
