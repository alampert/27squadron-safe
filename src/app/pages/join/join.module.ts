import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JoinRoutingModule } from './join-routing.module';
import { JoinComponent } from './join.component';
import { HowToJoinPage } from './cadet-how-to-join/how-to-join.page';
import { SharedModule } from '../../components/shared.module';
import { NewAccountPage } from 'app/pages/join/newaccount/new-account.page';

@NgModule({
	imports: [
		CommonModule,
		JoinRoutingModule,
		SharedModule
	], declarations: [
		JoinComponent,
		HowToJoinPage,
		NewAccountPage
	], schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JoinModule {
}
