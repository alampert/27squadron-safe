import { NgModule } from '@angular/core';
import { JoinComponent } from './join.component';
import { RouterModule, Routes } from '@angular/router';
import { HowToJoinPage } from './cadet-how-to-join/how-to-join.page';
import { NewAccountPage } from 'app/pages/join/newaccount/new-account.page';

const joinRoutes: Routes = [
	{
		path: 'join', component: JoinComponent, children: [
			{ path: '', redirectTo: '/join/cadet-how-to-join', pathMatch: 'full' },
			{ path: 'cadet-how-to-join', component: HowToJoinPage },
			{ path: 'newaccount', component: NewAccountPage }
		]
	}
];

@NgModule({
	imports: [
		RouterModule.forChild(joinRoutes)
	], exports: [
		RouterModule
	]
})
export class JoinRoutingModule {
}
