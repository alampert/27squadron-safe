import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../models/user.model';
import { UserService } from '../../../services/user/user.service';
import { RankModel } from '../../../models/rank.model';

@Component ( {
	selector    : 'app-howto',
	templateUrl : './how-to-join.page.html',
	styleUrls   : [ './how-to-join.page.css' ],
	providers   : [ UserService ]
} )
export class HowToJoinPage implements OnInit {

	public adminO : UserModel;

	constructor( private userService : UserService ) {
		this.adminO                     = new UserModel;
		this.adminO.rank                = new RankModel;
		this.adminO.rank.rank_text_full = '';
	}

	ngOnInit() {
		this.getAdminO ();
	}

	getAdminO() : void {
		this.userService.getUserByPosition ( 5 ).subscribe ( admino => {
			this.adminO = admino;
		} )
	}
}
