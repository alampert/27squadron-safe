import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../models/user.model';
import { AboutStaffService } from '../services/about-staff/about.staff.service';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import * as d3 from 'd3';
import * as TreeModel from 'tree-model';
import { UserService } from '../../../services/user/user.service';

@Component({
	selector: 'app-staff',
	templateUrl: 'about.staff.page.html',
	styleUrls: ['about.staff.page.css'],
	providers: [
		AboutStaffService,
		UserService
	]
})
export class AboutStaffPage implements OnInit {

	public staff_list: UserModel[];
	protected root: any;

	constructor(private aboutStaffService: AboutStaffService, private toastyService: ToastyService,
		private toastyConfig: ToastyConfig, private userService: UserService) {
		this.staff_list = new Array<UserModel>();
		this.toastyConfig.theme = 'material';
	}

	ngOnInit() {
		// Loading all staff into staff_list
		try {
			this.aboutStaffService.getStaff().subscribe(res_staff => {

				if (!res_staff) {
					throw new Error('No staff members were returned from database query');
				}

				res_staff.forEach(staff => {
					this.staff_list.push(staff);
				});

				this.root = this.buildTreeFromStaff(this.staff_list);

				let converted_root = this.convertTreeModelToD3(this.root);

				this.genTree(converted_root);

			});

		} catch (e) {
			console.error(e);
			this.toastyService.error(e.toString());
		}
	}

	buildTreeFromStaff(members: UserModel[]) {

		if (members == null) {
			throw new Error('members is null');
		}

		let co = members.find(u => u.position_id == 1);

		if (co == null) {
			console.debug(`Members`, members);
			throw new Error(`CO was not found!`);
		}

		let tree_root = (
			this.rec_appendStaff(co, members)
		);

		return tree_root;
	}

	treeParseMember(member: UserModel): any {

		if (member.member_id == null) {
			throw new Error('member_id was not passed in');
		}

		let treeModel = new TreeModel();
		return treeModel.parse({ 'id': member.member_id, 'member': member });
	}

	rec_appendStaff(parentStaff: UserModel, allStaff: UserModel[]) {

		if (parentStaff == null) {
			throw new Error(`parentStaff was null`);
		}

		if (parentStaff.position_id == null) {
			throw new Error(`parentStaff.position_id was null`);
		}

		let staffReportsTo = allStaff.filter(staff => parentStaff.position_id === staff.reportsTo);

		let root = this.treeParseMember(parentStaff);

		if (staffReportsTo.length > 0) {

			staffReportsTo.forEach(staff => {
				if (staff != null) {
					root.addChild(this.rec_appendStaff(staff, allStaff));
				}
			});

			return root;

		} else {

			return root;

		}
	}

	/**
	 * Takes in a node, and converts it into a D3 Readable format
	 * @param node
	 * @returns {any}
	 */
	convertTreeModelToD3(node: any) {

		let converted_node = {
			'name': node.model.member.rank_text_short + ' ' + node.model.member.fname[0] + '. '
			+ node.model.member.lname,
			'position': node.model.member.title_short,
			'children': []
		};

		if (node.children.length > 0) {

			node.children.forEach(child => {
				converted_node.children.push(this.convertTreeModelToD3(child));
			});

			return converted_node;

		} else {

			return converted_node;
		}
	}

	genTree(treeData: any) {

		// set the dimensions and margins of the diagram
		let levels = 4;	// Number of levels in diagram
		let margin = { top: 70, right: 90, bottom: 70, left: 90 };
		let width = 1000 - margin.left - margin.right;
		let height = (levels * 200) - margin.top - margin.bottom;

		// declares a tree layout and assigns the size
		let treemap = d3.tree()
			.size([
				width,
				height
			]);
		//  assigns the data to a hierarchy using parent-child relationships
		let nodes = d3.hierarchy(treeData);
		// maps the node data to the tree layout
		nodes = treemap(nodes);

		// append the svg obgect to the body of the page
		// appends a 'group' element to 'svg'
		// moves the 'group' element to the top left margin
		let svg = d3.select('app-staff').append('svg')
			.attr('width', '100%')
			.attr('height', height + margin.top + margin.bottom), g = svg.append('g')
				.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
		// adds the links between the nodes
		let link = g.selectAll('.link')
			.data(nodes.descendants().slice(1))
			.enter().append('path')
			.attr('class', 'link')
			.attr('d', function (d) {
				return 'M' + d.x + ',' + d.y + 'C' + d.x + ',' + (
					d.y + d.parent.y
				) / 2 + ' ' + d.parent.x + ',' + (
					d.y + d.parent.y
				) / 2 + ' ' + d.parent.x + ',' + d.parent.y;
			})
			.style('stroke', function (d, i) {
				return 'red';
			})
			.attr('fill', 'none');
		// adds each node as a group
		let node = g.selectAll('.node')
			.data(nodes.descendants())
			.enter().append('g')
			.attr('class', function (d) {
				return 'node' + (
					d.children ? ' node--internal' : ' node--leaf'
				);
			})
			.attr('transform', function (d) {
				return 'translate(' + d.x + ',' + d.y + ')';
			});
		// adds the circle to the node
		node.append('circle')
			.attr('r', 15);

		// adds the text to the node
		node.append('text')
			.attr('dy', '1em')
			.attr('y', function (d) {
				return d.children ? -40 : 40;
			})
			.style('text-anchor', 'middle')
			.text(function (d) {
				return d.data.name;
			});

		node.append('text')
			.attr('dy', '1em')
			.attr('y', function (d) {
				return d.children ? -60 : 60;
			})
			.style('text-anchor', 'middle')
			.text(function (d) {
				return d.data.position;
			});
	}

}
