import { RouterModule, Routes } from "@angular/router";
import { AboutProgramPage } from "./program/about.program.page";
import { AboutStaffPage } from "./staff/about.staff.page";
import { AboutSponsorsPage } from "./sponsors/sponsors.page";
import { AboutCommunityPage } from "./community/community.page";
import { AboutComponent } from "./about.component";
import { NgModule } from "@angular/core";
/**
 * Created by andrew on 24/04/17.
 */
const aboutRoutes : Routes = [
	{
		path : 'about', component : AboutComponent, children : [
		{ path : '', redirectTo : "/about/program", pathMatch : "full" },
		{ path : 'program', component : AboutProgramPage },
		{ path : 'staff', component : AboutStaffPage },
		{ path : 'sponsors', component : AboutSponsorsPage },
		{ path : 'community', component : AboutCommunityPage }
	]
	}
];
@NgModule ( {
	imports    : [
		RouterModule.forChild ( aboutRoutes )
	], exports : [
		RouterModule
	]
} )
export class AboutRoutingModule {
}