import { Component, OnInit } from "@angular/core";
import { environment } from "../../../../environments/environment";

@Component ( {
	selector : 'app-sponsors', templateUrl : 'sponsors.page.html', styleUrls : [ 'sponsors.page.css' ]
} )
export class AboutSponsorsPage implements OnInit {

	sponsors : any[];

	constructor() {
		this.sponsors = [
			{
				title            : "Branch 533 (Springbank Branch), Royal Canadian Legion",
				description      : "Branch 533 have been active supporters of this squadron for more than 20 years. They provide both monetary and resource support for squadron training. In turn, cadets participate in Legion community activities, including Remembrance Day parades, war graves maintenance, and the sale of poppies.",
				imgsrc           : "legionlogo.gif",
				html_title_short : "legion533"
			}, {
				title            : "427 (London) Wing, Air Force Association of Canada",
				description      : "The members of the 427 (London) Wing, Air Force Association of Canada have been ardent supporters of the Air Cadet squadrons in London and surrounding area for many years. Executive members regularly visit squadrons and participate in ceremonial activities, maintaining the tradition of the founding partner, the Royal Canadian Air Force.",
				imgsrc           : "RCAFA.gif",
				html_title_short : "afac427"
			}, {
				title            : "The Trillium Foundation",
				description      : "The Trillium Foundation is an agency of the Ontario Government responsible for awarding grants to non-profit organizations throughout the province. This squadron is but one of the many organizations to have received funding form Trillium in both 2006 and 2008. For more information, visit the Foundation's website.",
				imgsrc           : "TrilliumLogo.jpg",
				html_title_short : "trillium"
			}
		];
	}

	ngOnInit() {
	}

	getImagePath( path : string ) : string {
		return environment.serverBaseURL + "/assets/images/sponsors/" + path;
	}

}
