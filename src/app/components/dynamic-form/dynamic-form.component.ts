import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuestionControlService } from 'app/services/question-control/question-control.service';
import { QuestionBase } from 'app/components/questions/question-base';
import { ToastyService } from 'ng2-toasty';

@Component({
	selector: 'dynamic-form',
	templateUrl: './dynamic-form.component.html',
	providers: [QuestionControlService]
})
export class DynamicFormComponent implements OnInit {

	@Input() questions: QuestionBase<any>[] = [];
	@Input() buttonText: string;
	form: FormGroup;
	@Output() data = new EventEmitter<string>();
	submittingForm = false;

	constructor(private qcs: QuestionControlService, private toastyService: ToastyService) {
	}

	ngOnInit() {
		this.form = this.qcs.toFormGroup(this.questions);
		if (this.buttonText == null || this.buttonText == '') {
			this.buttonText = 'Submit';
		}
	}

	keyDownFunction(event) {
		// Setting default action for pressing 'enter' to submit form
		if (event.keyCode == 13) {
			this.onSubmit();
			event.stopPropagation();
		}
	}

	onSubmit() {
		if (!this.submittingForm) {
			try {
				this.submittingForm = true;
				let oldForm = this.qcs.toFormGroup(this.questions);
				let setValObj = {};

				for (let key in this.form.value) {

					if (!!(key)) {

						let value = typeof this.form.value[key] != 'undefined' ? this.form.value[key] : oldForm.get(key).value;
						let controlType = this.questions.find(q => q.key == key).controlType;

						setValObj[key] = value != '' ? value : '';
					}

				}

				oldForm.setValue(setValObj);

				this.data.emit(JSON.stringify(oldForm.value));

				this.submittingForm = false;

			} catch (e) {
				this.toastyService.error(e.message);
				console.error(e);
			}
		}
	}
}
