import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation/navigation.component';
import { MemberCardComponent } from './member-card/member-card.component';
import { EventCardComponent } from './event-card/event-card.component';
import { AppRoutingModule } from '../app-routing.module';
import { BsDropdownModule, TimepickerModule } from 'ngx-bootstrap';
import { StaffComponent } from '../pages/members/staff/staff.component';
import { ToastyModule } from 'ng2-toasty';
import { FooterComponent } from './footer/footer.component';
import { NotificationComponent } from './notification/notification.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { } from '@angular/material';
import { LoadingModalComponent } from './loading-modal/loading-modal.component';
import { ConfirmDeleteComponent } from 'app/pages/members/components/confirm-event-delete/confirm-delete.component';
import { DynamicFormComponent } from 'app/components/dynamic-form/dynamic-form.component';
import { DynamicFormQuestionComponent } from 'app/components/dynamic-form-question/dynamic-form-question.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from 'app/pages/members/components/change-password/change-password.component';
import { MatTabsModule, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule, MatSelectModule, MatDialogModule, MatCardModule, MatButtonModule, MatProgressBarModule, MatCheckboxModule, MatInputModule, MatMenuModule, MatToolbarModule, MatChipsModule, MatFormFieldModule, DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material';
import { MyDateAdapter, MY_DATE_FORMATS } from 'app/globals';

@NgModule({
	imports: [
		CommonModule,
		BsDropdownModule.forRoot(),
		AppRoutingModule,
		ToastyModule.forRoot(),
		MatDatepickerModule,
		MatNativeDateModule,
		MatProgressSpinnerModule,
		MatSelectModule,
		MatDialogModule,
		MatCardModule,
		MatButtonModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatInputModule,
		MatMenuModule,
		MatToolbarModule,
		MatChipsModule,
		ReactiveFormsModule,
		TimepickerModule.forRoot(),
		MatTabsModule,
		MatFormFieldModule
	], declarations: [
		NavigationComponent,
		MemberCardComponent,
		EventCardComponent,
		StaffComponent,
		FooterComponent,
		NotificationComponent,
		NotificationsComponent,
		LoadingModalComponent,
		DynamicFormQuestionComponent,
		DynamicFormComponent,
		ChangePasswordComponent
	], exports: [
		NavigationComponent,
		MemberCardComponent,
		EventCardComponent,
		FooterComponent,
		NotificationComponent,
		NotificationsComponent,
		MatDatepickerModule,
		MatNativeDateModule,
		MatProgressSpinnerModule,
		MatSelectModule,
		MatDialogModule,
		MatCardModule,
		MatButtonModule,
		MatProgressBarModule,
		MatCheckboxModule,
		MatInputModule,
		MatMenuModule,
		MatToolbarModule,
		MatChipsModule,
		DynamicFormQuestionComponent,
		DynamicFormComponent,
		ChangePasswordComponent,
		MatTabsModule,
		MatFormFieldModule
	], schemas: [CUSTOM_ELEMENTS_SCHEMA],
	entryComponents: [
		LoadingModalComponent,
		ConfirmDeleteComponent
	], providers: [
		{ provide: DateAdapter, useClass: MyDateAdapter },
		{ provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
	]
})
export class SharedModule {
}
