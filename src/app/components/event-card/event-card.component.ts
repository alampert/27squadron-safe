import { Component, OnInit, Input } from '@angular/core';
import { EventModel } from '../../models/event.model';
import { environment } from '../../../environments/environment';

/**
 * EventCardComponent displays a summary of an event in bootstrap's card format.
 */
@Component ( {
	selector    : 'app-event-card',
	templateUrl : './event-card.component.html',
	styleUrls   : [ './event-card.component.css' ]
} )
export class EventCardComponent implements OnInit {

	/**
	 * Takes in an EventModel to be displayed in the card.
	 */
	@Input()
	event : EventModel;

	/**
	 * Constructor does nothing at the moment.
	 */
	constructor() {
	}

	/**
	 * NgOnInit does nothing at the moment.
	 */
	ngOnInit() {
	}

	/**
	 * getPath takes in a file name, and returns the path needed for web browsers to display the image.
	 * @param img_path
	 * @returns {string}
	 */
	getPath( img_path : string ) : string {
		return environment.serverBaseURL + '/assets/images/inthecommunity/' + img_path;
	}
}
