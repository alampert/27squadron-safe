import { QuestionBase } from './question-base';

export class PasswordQuestion extends QuestionBase<string> {
	controlType = 'password';
	type: string;

	constructor(options: {} = {}) {
		super(options);
		this.type = options['type'] || '';
	}
}
