import { QuestionBase } from './question-base';

export class TimeQuestion extends QuestionBase<string> {
	controlType = 'time';

	constructor(options: {} = {}) {
		super(options);
	}
}
