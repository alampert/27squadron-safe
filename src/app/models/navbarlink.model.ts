/**
 * Created by andrew on 26/03/17.
 */

export class NavbarLinkModel {
	title : string;
	path? : string;
	nav_id?: string;
	sublinks? : NavbarLinkModel[];
};
