/**
 * Outlines all the details the rank objects can hold.
 */
export class RankModel {
	/**
	 * Rank ID is the id stored in the database for the current rank configuration
	 */
	rank_id ? : number;
	/**
	 * Full text of the rank
	 * @example <caption>Text example</caption>
	 * rank_text_full = "Flight Sergeant"
	 */
	rank_text_full : string;
	/**
	 * Short form of the rank. Used when using the abbreviated version of the name
	 */
	rank_text_short : string;
	/**
	 * Used to show equal ranks between applicable elements.
	 */
	authority_level : number;
	/**
	 * Element of the rank.
	 */
	element : string;
	/**
	 * Branch related to rank, such as CIC
	 */
	branch : string;
};
