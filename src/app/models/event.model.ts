/**
 * Created by andrew on 11/04/17.
 */

import { PublicStatus, ParentPermission, AvailabilityStatus } from '../globals';
import { UserModel } from 'app/models/user.model';
/**
 * Created by andrew on 11/04/17.
 */

export class EventModel {
	event_id?: number;
	event_title: string;
	event_description: string;
	start_date_time: string;
	end_date_time: string;
	is_public: PublicStatus;
	img_src?: string;
	parent_permission: ParentPermission;
	cadet_uniform: string;
	staff_uniform: string;
	max_cadet_count: number;
	attending_male_staff: number;
	attending_female_staff: number;
	attending_male_cadets: number;
	attending_female_cadets: number;
	attending_male_seniors: number;
	attending_female_seniors: number;
	additional_permission_form_notes: string;
	gen_permission_form: boolean;

	opi_member_id?: number;
	location?: string;

};

export class EventAvailModel extends EventModel {
	availability_id?: number;
	member_id?: number;
	status?: AvailabilityStatus;
	get_availabilities?: boolean;
	can_change?: boolean;
	other_cadets_attending?: number;
}

export class TrainingEventModel {
	id: number;						// Class ID
	title: string;					// Class title
	instructor_member_id?: number;	// Instructor for class
	instructor?: UserModel;
	start?: string;					// Start date/time for class
	end?: string;					// End date/time for class
	event_id?: number;				// Event ID that class is associated with
	// Grouping restrictions.
	// A member must be associated with all of the restrictions set below. If none are set, everyone can see the class. Staff always see classes.
	level?: number;
	group_id?: number
}
