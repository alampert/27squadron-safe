import { Injectable } from '@angular/core';
import { TrainingEventModel } from 'app/models/event.model';
import { Observable } from 'rxjs/Observable';
import { UserService } from 'app/services/user/user.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Http } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import { BaseService } from 'app/services/base/base.service';
import { environment } from '../../../environments/environment';
import { UserModel } from 'app/models/user.model';

@Injectable()
export class TrainingService extends BaseService {

	constructor(
		protected http: Http,
		protected toastyService: ToastyService,
		private userService: UserService,
		private dialog: MatDialog
	) {
		super(http, toastyService);
		this.dbUrl = environment.serverBaseURL + '/server/lib/training.php';
	}

	getAllClasses(): Observable<TrainingEventModel[]> {

		return this.http.get(this.dbUrl + '?query=getevents')
			.map(rawServerResponse => {

				if (rawServerResponse != null) {

					let serverResponse = rawServerResponse.json();

					return serverResponse;
				}

			});
	}

	getAllTestClasses(): Observable<TrainingEventModel[]> {
		let classes: TrainingEventModel[] = new Array<TrainingEventModel>();

		for (let i = 0; i < 10; i++) {
			classes.push({
				id: 0,
				title: 'Test Class',
				instructor_member_id: Math.round(Math.random() * 9),
			});
		}

		classes.map(cadet_class => {
			this.userService.getUserById(cadet_class.instructor_member_id)
				.subscribe(instructor => {
					cadet_class.instructor = instructor;
				});
		});

		return new Observable(subscriber => {
			subscriber.next(classes);
			subscriber.complete();
		});
	}

	getEvent(eventId: number): Observable<TrainingEventModel> {
		return this.http.get(this.dbUrl + '?query=getevent&eventid=' + eventId).map(rawServerResponse => {

			if (rawServerResponse.text().substr(0, 1) === 'S') {
				console.error(rawServerResponse.text());
				throw new Error('Server Error: ' + rawServerResponse.text());
			}

			let serverResponse = rawServerResponse.json();

			let trgEvent = new TrainingEventModel();

			trgEvent.id = serverResponse['id'];

			return trgEvent;
		});
	}

}
