import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { NavbarLinkModel } from '../../models/navbarlink.model';
import { BaseService } from '../base/base.service';
import { Http } from '@angular/http';
import { ToastyService } from 'ng2-toasty';

/**
 * NavigationService provies information to all navigational tools (mainly nav bars).
 */

@Injectable()
export class NavigationService extends BaseService {

	private navArray_public: NavbarLinkModel[];
	private navArray_members: NavbarLinkModel[];
	private navArray_staff: NavbarLinkModel[];

	/**
	 * Initializes all variables, and calls buildNav
	 */
	constructor(protected http: Http, protected toastyService: ToastyService) {

		super(http, toastyService);

		this.navArray_public = [
			{
				title: 'Home', path: './'
			},
			{
				title: 'About', sublinks: [
					{
						title: 'The Air Cadet Program', path: './about/program', nav_id: 'nav_theaircadetprogram'
					},
					{
						title: 'Staff', path: './about/staff', nav_id:'nav_staff'
					},
					{
						title: 'Sponsors', path: './about/sponsors', nav_id:'nav_sponsors'
					},
					{
						title: 'In the Community', path: './about/community', nav_id:'nav_inthecommunity'
					}
				]
			},
			{
				title: 'History', sublinks: [
					{
						title: 'Air Cadet History', path: './history/cadets', nav_id: 'nav_aircadethistory'
					},
					{
						title: 'Squadron History', path: './history/squadron', nav_id: 'nav_squadronhistory'
					},
					// {
					// 	title : 'Alumni', path : './history/alumni'
					// },
					// {
					// 	title : 'Photo Gallery', path : './history/gallery'
					// }
				]
			},
			{
				title: 'Join', sublinks: [
					{
						title: 'How To Join', path: './join/cadet-how-to-join'
					}
				]
			},
			{
				title: 'Members', path: './login'
			}
		];

		this.navArray_members = [
			{
				title: 'Dashboard', path: '/members/dashboard'
			},
			{
				title: 'Availabilities', path: '/members/availabilities'
			},
			// {
			// 	title : 'Supply', path : '/members/supply'
			// },
			{
				title: 'Profile', path: '/members/profile'
			}
		];

		this.navArray_staff = [
			{
				title: 'Staff', sublinks: [
					{
						title: 'Event Manager', path: '/members/staff/event-manager'
					},
					// {
					// 	title : 'Sign In Authenticator', path : '/members/staff/sia'
					// },
					{
						title: 'Member Manager', path: '/members/staff/member-manager'
					},
					{
						title: 'Training Manager', path: '/members/staff/training-manager'
					}
				]
			}
		];
	}

	public getPublicNav() {
		return this.navArray_public;
	}

	public getMemberNav() {
		return this.navArray_members;
	}

	public getStaffNav() {
		return this.navArray_staff;
	}

}
