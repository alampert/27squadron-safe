import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from 'app/services/base/base.service';
import { ToastyService } from 'ng2-toasty';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';
import { Observable } from 'rxjs/Observable';
import { GroupModel } from 'app/models/group.model';
import { UserService } from 'app/services/user/user.service';

@Injectable()
export class GroupsService extends BaseService {


	constructor(protected http: Http, protected toastyService: ToastyService, private notifService: NotificationsService, private userService: UserService) {
		super(http, toastyService);
		this.dbUrl = this.libsUrl + 'groups.php';
	}

	getAllGroups(member_id: number): Observable<GroupModel[]> {

		if (typeof +member_id !== 'number' || isNaN(member_id)) {
			throw new Error(`member_id (${member_id}) was not a number in GroupsService.getAllGroups(...). typeof member_id = ${typeof member_id}`);
		}

		return this.http.get(this.dbUrl + '?getallactivegroups=1&member_id=' + member_id).map(groups => {
			return groups.json().map(group => {

				let groupMembership = group.isMember === '1';

				let newGroup: GroupModel = {
					PI: this.userService.formatUserResponse(group),
					group_id: group.group_id,
					title: group.title,
					description: group.description,
					active_start: group.active_start,
					active_end: group.active_end,
					isMember: groupMembership
				}

				return newGroup;
			});
		});
	}

	updateGroupMemberships(member_id: number, groupConfig: GroupModel[]): Observable<any> {
		return this.http.post(this.dbUrl + '?updateMembership&member_id=' + member_id, groupConfig).map(groups => {
			// console.log(groups);
		});
	}

}
