import { inject, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { ToastyService, ToastyConfig } from 'ng2-toasty';
import { UserService } from 'app/services/user/user.service';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';
import { EventsService } from 'app/services/events/events.service';
import { EventModel } from 'app/models/event.model';

describe('Events Service', function () {

	let service;

	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				EventsService,
				ToastyService,
				ToastyConfig,
				UserService,
				NotificationsService
			],
			imports: [
				HttpModule
			]
		})
	});

	beforeEach(inject([EventsService], s => {
		this.service = s;
	}));

	it('should initiate', inject([EventsService], (s: EventsService) => {
		expect(s).toBeTruthy();
	}));

	describe('getevents', function () {

		it('should be able to get a list of all events', inject([EventsService], (s: EventsService) => {
			return s.getEvents().toPromise().then(events => {
				expect(events).toBeTruthy();
				expect(events.length).toBeGreaterThan(0);
			})
		}));

	});

	describe('staffRequirementMet', function () {

		let testEvent: EventModel;

		beforeAll(() => {
			testEvent = new EventModel();
		});

		it('should show passing for 1 officer / cadet - day', inject([EventsService], (s: EventsService) => {
			let day = new Date();
			testEvent.start_date_time = day.toString();
			testEvent.end_date_time = day.toString();

			testEvent.attending_male_cadets = 10;
			testEvent.attending_female_cadets = 10;
			testEvent.attending_male_staff = 10;
			testEvent.attending_female_staff = 10;

			expect(s.staffRequirementMet(testEvent)).toBe(true);

		}));

		it('should show passing for 1 officer / cadet - night', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setDate(day1.getDay() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			testEvent.attending_male_cadets = 10;
			testEvent.attending_female_cadets = 10;
			testEvent.attending_male_staff = 10;
			testEvent.attending_female_staff = 10;

			expect(s.staffRequirementMet(testEvent)).toBe(true);

		}));

		it('should show passing for 1 officer / 20 cadets - day', inject([EventsService], (s: EventsService) => {
			let day = new Date();
			testEvent.start_date_time = day.toString();
			testEvent.end_date_time = day.toString();

			testEvent.attending_male_cadets = 10;
			testEvent.attending_female_cadets = 10;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 1;

			expect(s.staffRequirementMet(testEvent)).toBe(true);

		}));

		it('should show passing for 1 officer / 15 cadets - night', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setDate(day1.getDay() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			testEvent.attending_male_cadets = 15;
			testEvent.attending_female_cadets = 15;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 1;

			expect(s.staffRequirementMet(testEvent)).toBe(true);

		}));

		it('should show failing for 1 officer / 100 cadets - day', inject([EventsService], (s: EventsService) => {
			let day = new Date();
			testEvent.start_date_time = day.toString();
			testEvent.end_date_time = day.toString();

			testEvent.attending_male_cadets = 50;
			testEvent.attending_female_cadets = 50;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 0;

			expect(s.staffRequirementMet(testEvent)).toBe(false);

		}));

		it('should show failing for 1 officer / 100 cadets - night', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setDate(day1.getDay() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			testEvent.attending_male_cadets = 50;
			testEvent.attending_female_cadets = 50;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 0;

			expect(s.staffRequirementMet(testEvent)).toBe(false);

		}));

		it('should show failing for 1 officer / 20 male cadets + 1 female cadet - day', inject([EventsService], (s: EventsService) => {
			let day = new Date();
			testEvent.start_date_time = day.toString();
			testEvent.end_date_time = day.toString();

			testEvent.attending_male_cadets = 20;
			testEvent.attending_female_cadets = 1;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 0;

			expect(s.staffRequirementMet(testEvent)).toBe(false);

		}));

		it('should show failing for 1 officer / 20 female cadets + 1 male cadet - day', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setDate(day1.getDay() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			testEvent.attending_male_cadets = 1;
			testEvent.attending_female_cadets = 20;
			testEvent.attending_male_staff = 1;
			testEvent.attending_female_staff = 0;

			expect(s.staffRequirementMet(testEvent)).toBe(false);

		}));

	});

	describe('isSameDay Method', function () {

		let testEvent: EventModel;

		beforeAll(() => {
			testEvent = new EventModel();
		});

		it('The same date should return true', inject([EventsService], (s: EventsService) => {
			let day = new Date();
			testEvent.start_date_time = day.toString();
			testEvent.end_date_time = day.toString();

			expect(s.isSameDay(testEvent)).toBe(true);
		}));


		it('Different Day should be false', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setDate(day2.getDate() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			expect(s.isSameDay(testEvent)).toBe(false);
		}));


		it('Different Month should be false', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setMonth(day2.getMonth() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			expect(s.isSameDay(testEvent)).toBe(false);
		}));


		it('Different Year should be false', inject([EventsService], (s: EventsService) => {
			let day1 = new Date();
			let day2 = new Date();
			day2.setFullYear(day2.getFullYear() + 1);
			testEvent.start_date_time = day1.toString();
			testEvent.end_date_time = day2.toString();

			expect(s.isSameDay(testEvent)).toBe(false);
		}));

	});


});
