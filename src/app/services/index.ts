import { NgModule, ModuleWithProviders } from '@angular/core';
import { FacebookService } from '../services/facebook/facebook.service';
import { UserService } from '../services/user/user.service';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';
import { AuthStaffService } from 'app/services/auth-user/auth-staff.service';
import { TrainingService } from 'app/services/training/training.service';
import { GroupsService } from 'app/services/groups/groups.service';
import { QuestionControlService } from 'app/services/question-control/question-control.service';


@NgModule({
	imports: [],
	exports: [],
	declarations: [],
	providers: [],
})
export class ServicesModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: ServicesModule,
			providers: [
				FacebookService,
				UserService,
				NotificationsService,
				AuthStaffService,
				TrainingService,
				GroupsService,
				QuestionControlService
			]
		};
	}
}
