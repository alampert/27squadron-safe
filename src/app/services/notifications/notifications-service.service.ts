import { Injectable, Input } from '@angular/core';
import { NotificationModel } from '../../models/notification.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class NotificationsService {

	notifications: NotificationModel[];

	constructor() {
		this.notifications = new Array<NotificationModel>();
		// this.addTestNotif();
	}

	getNotifications(): Observable<NotificationModel[]> {

		return new Observable(subscriber => {
			subscriber.next(this.notifications);
			subscriber.complete();

		});
	}

	addNotification(newNotif: NotificationModel, store: boolean = false) {
		this.notifications.push(newNotif);

		if (store) {
			console.warn('Permnanent storing is incomplete!');
		}
	}

	addTestNotif() {

		let testNotif = new NotificationModel;

		testNotif.text = 'This is a test notification. Please ignore!';
		testNotif.style = 'warning';

		this.addNotification(testNotif);

	}

}
