import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http, Response } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import { Observable } from 'rxjs/Observable';

/**
 * The Base Service is used to provide all services a set of standard functions and variables
 */
@Injectable()
export class BaseService {

	/**
	 * the baseURL is the url that is used to generate querries
	 */
	protected dbUrl: string;
	protected libsUrl: string;

	/**
	 * The base constructor sets the variables to default values. Can be overridden in the other services.
	 * @param http
	 */
	constructor(protected http: Http, protected toastyService: ToastyService) {
		this.libsUrl = environment.serverBaseURL + '/server/lib/';
		this.dbUrl = this.libsUrl + 'db.php';
	}

	protected handleError(error: any) {
		console.log('ERROR');
		console.error(error);
		this.toastyService.error(error.toString());
		return Observable.throw(error);
	}

	// Yahoo injects scripts, so this function is to be used to counter act said fuckery
	protected yahooFix(response: Response): any {
		let responseSplit = response.text().split(`<script type="text/javascript">`);
		return JSON.parse(responseSplit[0]) as any[];
	}

}
