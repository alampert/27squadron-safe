import { Injectable } from '@angular/core';
import { SearchUserModel, UserModel, BaseUserModel } from '../../models/user.model';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { BaseService } from '../base/base.service';
import { RankModel } from '../../models/rank.model';
import { Observable } from 'rxjs/Observable';
import { ToastyService } from 'ng2-toasty';
import { NotificationsService } from 'app/services/notifications/notifications-service.service';
import { NotificationModel } from 'app/models/notification.model';
import 'rxjs/add/operator/toPromise';
import { AuthUserService } from 'app/services/auth-user/auth-user.service';
import 'rxjs/add/operator/catch';

@Injectable()
export class UserService extends BaseService {

	private ranks: RankModel[];
	private localStorageTokenString: string;

	/**
	 * Initializes all the variables.
	 */
	constructor(protected http: Http, protected toastyService: ToastyService, private notifService: NotificationsService) {
		super(http, toastyService);
		this.dbUrl = this.libsUrl + 'members.php';
		this.ranks = new Array<RankModel>();
		this.localStorageTokenString = 'currentUser';
		let rank_sub = this.getAllRanks().subscribe(ranks => {
			if (!!ranks) {
				this.ranks = ranks;
				rank_sub.unsubscribe();
			}
		})
	}

	getUserById(member_id: number): Observable<UserModel> {

		if (typeof +member_id !== 'number' || isNaN(member_id)) {
			throw new Error(`member_id (${member_id}) was not a number in UserService.getUserById(...). typeof member_id = ${typeof member_id}`);
		}

		return this.http.get(this.dbUrl + '?query=getuser&member_id=' + member_id).map((res: Response) => {

			let member = this.yahooFix(res)[0];

			if (!!(this.ranks)) {
				member.rank = this.ranks.find(rank => member.rank_id == rank.rank_id);
			}

			return member;
		});
	}

	getUserByPosition(position_id: number): Observable<UserModel> {
		return this.http.get(this.dbUrl + '?query=getuserbypos&position_id=' + position_id)
			.map((res: Response) => this.formatUserResponse(this.yahooFix(res)[0]));
	}

	updateUser(newUser: UserModel): Observable<any> {

		if (newUser == null) {
			throw new Error('Member was not passed in correctly');
		}

		// Default values
		let mode = 'update';
		let serverUrl = this.libsUrl + 'profile.php' + '?query=' + mode + '&member_id=' + newUser.member_id;

		// Values for new user
		if (!newUser.member_id) {
			mode = 'new';
			serverUrl = (this.libsUrl + 'profile.php' + '?query=' + mode);
			newUser.rank_id = 1;
		}

		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });

		return this.http.post(serverUrl, { member: newUser }, options).map((res: Response) => {
			let response = this.yahooFix(res);
			if (response.error != null) {
				console.error(response.error);
				this.toastyService.error(response.error);
			}

			// Add temporary notification with temp password for users account if it has been returned

			if (typeof response[0] !== 'undefined') {
				if (typeof response[0].tmpPassword !== 'undefined') {
					let tmpPassNotif = new NotificationModel;
					tmpPassNotif.text = 'Temporary Password: ' + response[0].tmpPassword;
					tmpPassNotif.style = 'success';

					this.notifService.addNotification(tmpPassNotif);
				}
			}

			return response;

		}).catch((error, caught) => {
			return this.handleError(error);
		});
	}


	listOfUsers(): Observable<SearchUserModel[]> {
		return this.http.get(this.dbUrl + '?query=getlistofcurrentusers').map((rawUsers: Response) => {
			let userJson = rawUsers.json();
			return userJson.map(rawUser => {
				return this.formatUserResponse(rawUser);
			});
		});
	}

	getInstructors(): Observable<BaseUserModel[]> {
		return this.http.get(this.dbUrl + '?query=getlistofcurrentusers')
			.map((res: Response) => {
				return this.appendRankToSearchUserList(this.yahooFix(res));
			});
	}

	initializeMember(member_id: number): Observable<any> {
		let token: string = this.getStoredToken();
		return this.http.get(`${this.libsUrl}account_initialization.php?query=initialize&member_id=${member_id}&token=${token}`)
			.map((res: Response) => {
				return this.yahooFix(res);
			});
	}

	getAllRanks(): Observable<RankModel[]> {
		return this.http.get(this.dbUrl + '?query=getranks')
			.map((res: Response) => this.yahooFix(res).sort((a, b) => {
				if (+a.authority_level < +b.authority_level) {
					return -1;
				} else if (+a.authority_level === +b.authority_level) {
					if (a.element === 'Air') {
						return 1
					} else if (b.element === 'Air') {
						return -1
					} else {
						return 0;
					}
				} else if (+a.authority_level > +b.authority_level) {
					return 1;
				}
			}))
			.map(ranks => {
				this.ranks = ranks;
				return ranks;
			});
	}

	getRank(rank_id: number): Promise<RankModel> {
		if (this.ranks.length === 0) {
			return this.getAllRanks().toPromise().then(x => {

				this.ranks = x;

				return this.ranks.find(rank => {
					return rank.rank_id === rank_id;
				});
			});
		}

		return new Promise((resolve, reject) => {
			let rank_find = this.ranks.find(rank => {
				return rank.rank_id === rank_id;
			});

			resolve(rank_find);
		});
	}


	getStoredToken(): string {
		return !!(localStorage.getItem(this.localStorageTokenString)) ? JSON.parse(localStorage.getItem(this.localStorageTokenString)).token : null;
	}

	appendRankToSearchUserList(users: SearchUserModel[]): SearchUserModel[] {

		let promises = new Array<Promise<RankModel>>();

		users.forEach(user => {
			if (user.rank_id != null) {
				promises.push(this.getRank(user.rank_id));
			}
		});

		Promise.all(promises).then(getRanks => {

			for (let i = 0; i < getRanks.length; i++) {
				users[i].rank = <RankModel>getRanks[i];
			}

		});

		return users;
	}

	getStaffPositions(member_id: number): Observable<any> {
		return this.http.get(this.dbUrl + '?query=staff_positions&member_id=' + member_id)
			.map((res: Response) => this.yahooFix(res));
	}

	formatUserResponse(rawUser: any): UserModel {
		let filteredUser: UserModel;

		let filteredRank: RankModel;
		filteredRank = {
			rank_id: rawUser.rank_id,
			rank_text_full: rawUser.rank_text_full,
			rank_text_short: rawUser.rank_text_short,
			authority_level: rawUser.authority_level,
			element: rawUser.element,
			branch: rawUser.branch
		}

		filteredUser = {
			member_id: rawUser.member_id,
			fname: rawUser.fname,
			lname: rawUser.lname,
			rank_id: rawUser.rank_id,
			rank: filteredRank,

			gender: rawUser.gender,
			email: rawUser.email,
			phone_number: rawUser.phone_number,
			profile_image_filename: rawUser.profile_image_filename,
			archived: rawUser.archived,
			element: rawUser.element,
			branch: rawUser.branch,
			reportsTo: rawUser.reportsTo,
			position_id: rawUser.position_id,
			title: rawUser.title,
			initialized: rawUser.initialized == '1',

			level: rawUser.level
		}

		return filteredUser;
	}

	passwordRecovery(email: string): Observable<any> {
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });
		return this.http.post(
			`${this.dbUrl}?query=passwordrecover`,
			{ 'email': email },
			options
		)
			.map((res: Response) => {
				return res;
			});
	}

	updatePassword(member_id: number, email: string, password: string): Observable<any> {
		let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
		let options = new RequestOptions({ headers: headers, method: 'post' });
		return this.http.post(
			`${this.dbUrl}?query=setpassword`,
			{ 'email': email, 'member_id': member_id, 'password': password },
			options
		)
			.map((res: Response) => {
				return this.yahooFix(res);
			});
	}
}
