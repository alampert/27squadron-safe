import { Injectable } from '@angular/core';
import { BaseService } from '../base/base.service';
import { Http, RequestOptions, Response } from '@angular/http';
import { ToastyService } from 'ng2-toasty';

@Injectable()
export class FacebookService extends BaseService {

	facebookToken: string;
	facebookGroupId : string;
	facebookAppId : string;

	constructor(protected http: Http, protected toastyService: ToastyService) {
		super(http, toastyService);
		// tslint:disable-next-line:max-line-length
		this.facebookToken = 'EAACEdEose0cBAPnstNBHphoOsXeOYSylTnJUhvAwdH87hyZA1VG1HZBXZALZBOgfoVaCYZBX299VMRsKYwBCLvNOiLoMUwa6hVu4ZC7r2uUpl4zWrWtgpvgVThFYbmxByx8FBdB52QkZAKDsLKZBwI6d0PlXWN5MH8D0jilXKdHklotE4ZBX10UuUze7rM0R0Qc7WlJeHcoQ4pooLRVcOpY6quzVw8w8T1HHA3VCdBOOTOQZDZD';
		this.facebookGroupId = '680619088788748';
		this.facebookAppId = '1313016385485588';
	}

	groupPost(postContent: string) {
		console.debug('Facebook Service: Sending group post');
		let fixedPostContent = '27 Website: ' + postContent;
		return this.http.post(`https://graph.facebook.com/v2.5/${this.facebookGroupId}/feed?access_token=${this.facebookToken}`, {message : fixedPostContent})
			.map((res: Response) => {
				return res.json()
			})
			.catch(this.handleError);
	}

}
