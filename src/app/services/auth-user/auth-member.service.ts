/**
 * Created by andrew on 20/05/17.
 */
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthUserService } from './auth-user.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { ToastyConfig, ToastyService } from 'ng2-toasty';

@Injectable()
export class AuthMember implements CanActivate {

	private token: string;
	private username: string;
	private password: string;
	private member_id: number;

	constructor(private authUser: AuthUserService, private toastyConfig: ToastyConfig,
		private toastyService: ToastyService) {
		this.toastyConfig.theme = 'material';
		this.username = '';
		this.password = '';
		this.member_id = 0;
	}

	canActivate(): Observable<boolean> {

		let ls = JSON.parse(localStorage.getItem('currentUser'));

		// If token is already set, validate the token.
		if (this.token != null) {

			return this.validateToken();

		} else if (ls.token != null) {

			this.token = ls.token;
			return this.validateToken();

		} else if (this.token == null && this.username != null && this.password != null) {

			// Retrieving token if username and password are set
			this.updateToken(this.username, this.password).subscribe(token => {
				if (token == null) {
					throw new Error('Token that was returned from updateToken was null');
				}
				this.token = token;
				// Validating token
				return this.validateToken().subscribe();
			});

		} else {

			// If nothing worked, return false
			return new Observable(subscriber => {
				subscriber.next(false);
				subscriber.complete();
			});

		}
	}

	/**
	 * Update Token calls the authUser function to get the token, and sets the private member variable, token, to be the received token.
	 * This function returns a promise in order to allow other functions to resolve synchronously.
	 * @param username
	 * @param password
	 * @returns {Promise<TResult2|string>} token
	 */
	updateToken(username: string, password: string): Observable<string> {
		if (username == null || password == null || username === '' || password === '') {
			throw new Error('You need to submit a username and password');
		}

		this.username = username;

		return this.authUser.getToken(username, password).map(resp_token => {
			this.token = resp_token.token;
			this.member_id = resp_token.member_id;
			return this.token;
		});

	}

	validateToken(): Observable<boolean> {
		return this.authUser.validateToken(this.token);
	}

	logout(): void {

		// TODO: Send command to server to revoke current token

		delete this.username;
		delete this.password;
		delete this.token;
		this.authUser.logout();

		this.toastyService.warning('User has been logged out');
	}

	getUsername() {
		return this.username;
	}

	getMemberId(): number {
		return this.member_id;
	}

	getToken() {
		return this.token;
	}

}
