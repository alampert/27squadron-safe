/**
 * Created by andrew on 23/04/17.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './pages/home/home.page';
import { AboutProgramPage } from './pages/about/program/about.program.page';
import { AboutStaffPage } from './pages/about/staff/about.staff.page';
import { AboutSponsorsPage } from './pages/about/sponsors/sponsors.page';
import { AboutCommunityPage } from './pages/about/community/community.page';
import { HistoryCadetPage } from './pages/history/cadet/history.cadet.page';
import { PageNotFoundPage } from './pages/general/page-not-found/page-not-found.page';
import { MembersLoginPage } from './pages/login/login.page';
import { ChangeLogPage } from './pages/changelog/changelog.page';
import { ForgotPasswordPage } from 'app/pages/forgot-password/forgot-password.page';

const appRoutes: Routes = [
	{ path: 'home', component: HomePage },
	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'login', component: MembersLoginPage },
	{ path: 'forgotpassword', component: ForgotPasswordPage },
	{ path: 'changelog', component: ChangeLogPage }
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes, { useHash: true })
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule { }
