<?php

header("Access-Control-Allow-Origin: *");

include("db_fns.php");

$query = "";

try{

	if( isset($_GET['query']) ){

		if( $_GET['query'] == 'getevent' && isset($_GET['eventid']) ){
			$query = "SELECT
				event_id,
				event_title,
				event_description,
				max_cadet_count,
				start_date_time,
				end_date_time,
				img_src,
				is_public,
				parent_permission,
				cadet_uniform,
				staff_uniform,
				get_availabilities,
				events.can_change,
				events.additional_permission_form_notes,
				events.gen_permission_form
			FROM events
			WHERE event_id = " . $_GET['eventid'];

			$data = db_query($query);
		}

		else if( $_GET['query'] == "getevents" ){
			$query = "SELECT
				event_id,
				event_title,
				event_description,
				max_cadet_count,
				start_date_time,
				end_date_time,
				img_src,
				is_public,
				parent_permission,
				cadet_uniform,
				staff_uniform,
				get_availabilities,
				can_change,
				events.additional_permission_form_notes,
				events.gen_permission_form,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
				) AS attending,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = -1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
				) AS not_attending,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch != 'Cadet'
				) AS attending_staff,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = -1
					AND availabilities.event_id = events.event_id
					AND ranks.branch != 'Cadet'
				) AS not_attending_staff,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
					AND members.gender = 'male'
				) AS attending_male_cadets,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
					AND members.gender = 'female'
				) AS attending_female_cadets,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch != 'Cadet'
					AND members.gender = 'male'
				) AS attending_male_staff,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch != 'Cadet'
					AND members.gender = 'female'
				) AS attending_female_staff,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
					and ranks.authority_level >= 6
					AND members.gender = 'male'
				) AS attending_male_seniors,
				(
					SELECT
					COUNT(availabilities.status)
					FROM availabilities
					LEFT JOIN members ON availabilities.member_id = members.member_id
					LEFT JOIN ranks ON members.rank_id = ranks.rank_id
					WHERE availabilities.status = 1
					AND availabilities.event_id = events.event_id
					AND ranks.branch = 'Cadet'
					and ranks.authority_level >= 6
					AND members.gender = 'female'
				) AS attending_female_seniors
				FROM events
				ORDER BY events.start_date_time";

			$data = db_query($query);

		} else if ( $_GET['query'] == "setevent" ){


			// Parsing $_POST data

			$postdata = file_get_contents("php://input");
			$response = json_decode($postdata);

			$event = $response->event;

			// Error Checking

			if( !isset($event) ){
				throw new Exception("event was not set");
			}

			if( isset($event->event_id) ){

				if( !isset($event->event_title) ){
					throw new Exception("event title was not set");
				}
				if( !isset($event->event_id) || is_int($event->event_id) ){
					throw new Exception("event id was not set properly");
				}
				if( !isset($event->max_cadet_count) ){
					throw new Exception("event max_cadet_count was not set");
				}
				if( !isset($event->start_date_time) ){
					throw new Exception("event start_date_time was not set");
				}
				if( !isset($event->end_date_time) ){
					throw new Exception("event end_date_time was not set");
				}
				if( !isset($event->img_src) ){
					// img_src is optional
					// throw new Exception("event img_src was not set");
				}
				if( !isset($event->is_public) ){
					throw new Exception("event is_public was not set");
				}
				if( !isset($event->parent_permission) ){
					throw new Exception("event parent_permission was not set");
				}
				if( !isset($event->cadet_uniform) ){
					throw new Exception("event cadet uniform was not set");
				}
				if( !isset($event->staff_uniform) ){
					throw new Exception("event staff uniform was not set");
				}
				if( !isset($event->get_availabilities) ){
					throw new Exception("event get_availabilities was not set");
				}
				if( !isset($event->can_change) ){
					throw new Exception("event can_change was not set");
				}
				if( !isset($event->gen_permission_form) ){
					throw new Exception("event gen_permission_form was not set");
				}
				if( !isset($event->additional_permission_form_notes) ){
					throw new Exception("event additional_permission_form_notes was not set");
				}

				$description = '';
				$end_date = $event->start_date_time;

				if(isset($event->event_description)){
					$description = $event->event_description;
				}

				if(isset($event->end_date_time) && $event->end_date_time != ''){
					$end_date = $event->end_date_time;
				}

				$query = "UPDATE events SET
					event_title = \"" . $event->event_title . "\",
					event_description = \"" . $description . "\",
					max_cadet_count = \"" . $event->max_cadet_count . "\",
					start_date_time = \"" . $event->start_date_time . "\",
					end_date_time = \"" . $end_date . "\",
					img_src = \"" . $event->img_src . "\",
					is_public = \"" . $event->is_public . "\",
					parent_permission = \"" . $event->parent_permission . "\",
					cadet_uniform = \"" . $event->cadet_uniform . "\",
					staff_uniform = \"" . $event->staff_uniform . "\",
					get_availabilities = \"" . $event->get_availabilities . "\",
					can_change = \"" . $event->can_change . "\",
					gen_permission_form = \"" . $event->gen_permission_form . "\",
					additional_permission_form_notes = \"" . $event->additional_permission_form_notes . "\"
					WHERE event_id = " . (int)$event->event_id;

				db_query($query);

				$query = "SELECT * FROM events WHERE event_id = " . $event->event_id;

			} else {
				$query = "INSERT INTO events SET event_title = 'New Event';";
				db_query($query);

				// Getting latest entry (entry that was just added)
				$query = "SELECT * FROM `events` WHERE event_id IN (SELECT max(event_id) FROM events)";
			}

			$data = db_query($query);

		} else if ( $_GET['query'] == "deleteevent" ){

			if(!isset($_GET['event_id'])){
				throw new Exception("event_id was not set");
			}

			$query = "DELETE FROM events WHERE event_id = " . $_GET['event_id'];

			$data = db_query($query);

		}


	} else {

		throw new Exception("No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

echo(json_encode($data));
