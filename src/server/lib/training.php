<?php

header("Access-Control-Allow-Origin: *");

include("db_fns.php");

$query = "";

try{

	if( isset($_GET['query']) ){

		if( $_GET['query'] == 'getevent' && isset($_GET['eventid']) ){

			$data = getEvent($_GET['eventid']);

		} else if ($_GET['query'] == 'getevents'){

			$data = getClasses();

		}

	} else {

		throw new Exception("No Data");

	}

} catch (Exception $e){
	$data = array("error" => $e->getMessage());
}

echo(json_encode($data));

function getEvent($event_id){

	$query = "SELECT
		event_id,
		event_title,
		event_description,
		max_cadet_count,
		start_date_time,
		end_date_time,
		img_src,
		is_public,
		parent_permission,
		cadet_uniform,
		staff_uniform,
		get_availabilities,
		can_change
		FROM events
		WHERE event_id = " . $event_id . "
		ORDER BY events.start_date_time";

	return db_query($query);
}

function getClasses(){
	$query = "SELECT
		id,
		title,
		instructor_member_id,
		start,
		end,
		event_id,
		level,
		group_id
	FROM training_class";

	$classes = db_query($query);



	if(sizeof($classes) > 0){
		for($i = 0; $i < sizeof($classes); $i++){

			if($classes[$i]['instructor_member_id'] != null){

				$instructorQuery = "SELECT
						members.member_id,
						members.fname,
						members.lname,
						members.gender,
						members.rank_id,
						members.email,
						members.phone_number,
						members.element,
						members.branch,
						cadet_information.level,
						ranks.rank_id,
						ranks.rank_text_full,
						ranks.rank_text_short,
						ranks.authority_level,
						ranks.element,
						ranks.branch
					FROM members
					LEFT JOIN ranks ON ranks.rank_id = members.rank_id
					LEFT JOIN cadet_information ON cadet_information.member_id = members.member_id
					WHERE members.member_id = " . $classes[$i]['instructor_member_id'];

				$instructor = db_query($instructorQuery);

				$classes[$i]['instructor'] = $instructor[0];
			}
		}
	}

	return $classes;
}
