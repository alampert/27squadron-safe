<?php

include("db_fns.php");

// https://graph.facebook.com/v2.5/${this.facebookGroupId}/feed?access_token=${this.facebookToken}
$baseFBUrl = "https://graph.facebook.com/v2.9/";

// Get all messages that need to be posted to facebook
$getFBMsgsToPostQuery = "SELECT
	notifications.notification_id,
	notifications.title,
	notifications.text,
	notifications.display_start,
	notifications.display_end,
	notifications.private
	FROM notifications
	WHERE notifications.postToFacebook = 1
	AND notifications.postedToFacebook = 0;";

$msgs = db_query($getFBMsgsToPostQuery);

var_dump($msgs);

// TODO: Calculate length of all posts

// TODO: Upload posts in one big message, or split up
$data = array('message' => $msgs[0]['text']);
$fbUrl = $baseFBUrl . $fbGroupId . "/feed?message=" . $msgs[0]['text'] . "&access_token=" . $facebookToken;

$query = http_build_query ($data);

// Create Http context details
$contextData = array (
            'method' => 'POST',
            'header' => "Connection: close\r\n".
                        "Content-Length: ".strlen($query)."\r\n",
            'content'=> $query );

// Create context resource for our request
$context = stream_context_create (array ( 'http' => $contextData ));

// Read page rendered as result of your POST request
$result =  file_get_contents (
              $fbUrl,  // page url
              false,
              $context);

// Server response is now stored in $result variable so you can process it
echo($result);



// TODO: Mark messages as posted to facebook

