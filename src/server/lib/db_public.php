<?php

include("db_fns.php");


if( isset($_GET['query']) ){

	$query = "";

	// Preset Queries
	if( $_GET['query'] == 'current_staff_list' ){

		$query = "SELECT
				members.member_id,
				members.fname,
				members.lname,
				group_concat( title SEPARATOR ', ') AS title,
				group_concat( title_short SEPARATOR ', ') AS title_short,
				members.gender,
				ranks.rank_id,
				ranks.rank_text_full,
				ranks.rank_text_short,
                ranks.authority_level,
				members.profile_image_filename,
				MIN(staff_positions.reportsTo) AS reportsTo,
				MIN(staff.position_id) AS position_id
			FROM staff
			LEFT JOIN members ON staff.member_id = members.member_id
			LEFT JOIN staff_positions on staff.position_id = staff_positions.position_id
			LEFT JOIN ranks ON members.rank_id = ranks.rank_id
			WHERE (staff.start_date <= NOW() AND (
				IF(staff.end_date IS NULL, true, staff.end_date >= NOW())
			))
			GROUP BY staff.member_id
			ORDER BY reportsTo ASC, authority_level DESC";

			// SELECT
			// 	members.member_id,
			// 	members.fname,
			// 	members.lname,
			// 	group_concat( title SEPARATOR ', ') AS title,
			// 	members.gender,
			// 	members.email,
			// 	members.phone_number,
			// 	ranks.rank_text_full,
			// 	members.profile_image_filename,
			// 	staff_positions.reportsTo,
			// 	staff.position_id
			// FROM staff
   //          LEFT JOIN (SELECT staff_positions.position_id, MAX(staff_positions.reportsTo) as rt FROM staff_positions) t2
   //          ON staff.position_id = t2.position_id
			// LEFT JOIN members ON staff.member_id = members.member_id
			// LEFT JOIN staff_positions on staff.position_id = staff_positions.position_id
			// LEFT JOIN ranks ON members.rank_id = ranks.rank_id
			// WHERE (staff.start_date <= NOW() AND (
			// 	IF(staff.end_date IS NULL, true, staff.end_date >= NOW())
			// ))
			// GROUP BY staff.member_id
			// ORDER BY staff_positions.authority_level DESC, staff_positions.reportsTo DESC
	} else if( $_GET['query'] == 'past_cos' ){

		$query = "SELECT
				members.fname,
				members.lname,
				members.gender,
				ranks.rank_text_full,
				members.profile_image_filename,
				CONCAT('(', YEAR(staff.start_date), ' - ', YEAR(staff.end_date), ')') AS title
			FROM staff
			LEFT JOIN members ON staff.member_id = members.member_id
			LEFT JOIN staff_positions on staff.position_id = staff_positions.position_id
			LEFT JOIN ranks ON members.rank_id = ranks.rank_id
			WHERE (staff.start_date < NOW() AND staff.end_date < NOW()) AND staff_positions.position_id = 1
			GROUP BY staff.member_id
			ORDER BY staff.end_date DESC";
	}


	$data = db_query($query);

} else {

	$data = array("Error", "No Data");

}

echo(json_encode($data));
